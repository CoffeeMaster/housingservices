Request Management System
===

*RMS for Housing purposes*

***Developers installation guide***

---

##Prerequisites

 You need following products to be installed on your computer

* git v1.x VCS
* Oracle jdk v1.8
* maven v3.x
* Mysql v5.5 or later
* Intellij IDEA or Eclipse IDE

---

##Project configure for Intellij IDEA

1. Open git console or command line if you use Linux/MacOS. Clone current repository
to your local folder by writing command
`git clone https://bitbucket.org/CoffeeMaster/housingservices.git`

2. Open Intellij IDEA and go to "Project Structure" `Ctrl+Alt+Shift+S` ==> "Modules".  
    2.1 Make src folder ***not Source***.  
    2.2 Set src/main/java as ***Source*** folder.  
    2.3 Set src/main/resources as ***Resources*** folder.  
    2.4 Set src/main/test as ***Tests*** folder.  

3. Open src/main/resources/hibernate-mysql.properties file.

    change database username and password in following lines

    ```
    jdbc.user=root  
    jdbc.pass=root
    ```

4. Select pom.xml file in project root and mark it as *Maven* file.
   After this action import maven dependencies.

5. Run project as Spring Boot taking help of Spring Boot plugin.

    Alternative option is to run project with ***Maven***.  
    5.1 Go to "Edit Configurations"(Run Configurations) ==> 
    "Add New Configuration" ==> "Maven".  
    5.2 Set name for configuration  
    5.3 Set "Command Line" parameter to `spring-boot:run`  
    5.4 Apply changes and exit  
    5.4 Push "Run" button  

---

