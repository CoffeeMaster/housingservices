package il.co.hs4you.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 01.10.16.
 */

@Entity
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "town", nullable = true, length = 45)
    private String town;

    @Basic
    @Column(name = "street", nullable = true, length = 45)
    private String street;

    @Basic
    @Column(name = "house_numb", nullable = true, length = 8)
    private String houseNumb;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="region_id")
    private Region region;

    @OneToOne(mappedBy = "address")
    @PrimaryKeyJoinColumn
    @JsonIgnore
    private Building building;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumb() {
        return houseNumb;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public void setHouseNumb(String houseNumb) {
        this.houseNumb = houseNumb;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        if (id != address.id) return false;
        if (town != null ? !town.equals(address.town) : address.town != null) return false;
        if (street != null ? !street.equals(address.street) : address.street != null) return false;
        if (houseNumb != null ? !houseNumb.equals(address.houseNumb) : address.houseNumb != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (town != null ? town.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (houseNumb != null ? houseNumb.hashCode() : 0);
        return result;
    }
}
