package il.co.hs4you.model.entity;

/**
 * @author Matvey Mitnitsky
 * @since  02.11.16.
 */

public enum ApplicationStatus {
    newAppl,
    in_proccess,
    assigned,
    work_in_proccess,
    completed,
    reassigned
}
