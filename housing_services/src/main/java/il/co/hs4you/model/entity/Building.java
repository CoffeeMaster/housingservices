package il.co.hs4you.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 01.10.16.
 */

@Entity
@Table(name="building")
public class Building {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "floors", nullable = true, length = 45)
    private String floors;

    @Basic
    @Column(name = "found_date", nullable = true)
    private Date foundDate;

    @Basic
    @Column(name = "last_repair_date", nullable = true)
    private Date lastRepairDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="address_id")
    private Address address;

    @OneToMany(mappedBy = "building", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Flat> flats;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFloors() {
        return floors;
    }

    public void setFloors(String floors) {
        this.floors = floors;
    }

    public Date getFoundDate() {
        return foundDate;
    }

    public void setFoundDate(Date foundDate) {
        this.foundDate = foundDate;
    }

    public Date getLastRepairDate() {
        return lastRepairDate;
    }

    public void setLastRepairDate(Date lastRepairDate) {
        this.lastRepairDate = lastRepairDate;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    /*TODO fix fetch issue. failed to lazily initialize a collection
     * of role: il.co.hs4you.model.entity.Building.flats
     * could not initialize proxy - no Session
     */
    @OneToMany(mappedBy = "building", fetch = FetchType.LAZY)
    public List<Flat> getFlats() {
        return flats;
    }

    public void setFlats(List<Flat> flats) {
        this.flats = flats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Building building = (Building) o;

        if (id != building.id) return false;
        if (floors != null ? !floors.equals(building.floors) : building.floors != null) return false;
        if (foundDate != null ? !foundDate.equals(building.foundDate) : building.foundDate != null) return false;
        if (lastRepairDate != null ? !lastRepairDate.equals(building.lastRepairDate) : building.lastRepairDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (floors != null ? floors.hashCode() : 0);
        result = 31 * result + (foundDate != null ? foundDate.hashCode() : 0);
        result = 31 * result + (lastRepairDate != null ? lastRepairDate.hashCode() : 0);
        return result;
    }
}
