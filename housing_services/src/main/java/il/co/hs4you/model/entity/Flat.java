package il.co.hs4you.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 01.10.16.
 */

@Entity
@Table(name = "flat")
public class Flat {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "flat_number", nullable = false, columnDefinition = "int default 0")
    private int flatNumber;

    @Basic
    @Column(name = "rooms", nullable = false, columnDefinition = "int default 0")
    private int rooms;

    @Basic
    @Column(name = "balconies", nullable = false, columnDefinition = "int default 0")
    private int balconies;

    @Basic
    @Column(name = "square", nullable = false, precision = 1, columnDefinition = "double default 0.0")
    private double square;

    @ManyToOne(cascade = javax.persistence.CascadeType.ALL)
    @JoinColumn(name = "building_id")
    private Building building;

    @OneToMany(mappedBy = "flat", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Renter> renters;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public int getBalconies() {
        return balconies;
    }

    public void setBalconies(int balconies) {
        this.balconies = balconies;
    }

    public double getSquare() {
        return square;
    }

    public void setSquare(double square) {
        this.square = square;
    }

    public List<Renter> getRenters() {
        return renters;
    }

    public void setRenters(List<Renter> renters) {
        this.renters = renters;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(int flatNumber) {
        this.flatNumber = flatNumber;
    }

}
