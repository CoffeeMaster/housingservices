package il.co.hs4you.model.entity;

import javax.persistence.*;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 01.10.16.
 */


@Entity
@Table(name = "problem_place")
public class ProblemPlace {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "place", nullable = true, length = 45)
    private String place;

    public ProblemPlace(String place) {
        this.place = place;
    }

    public ProblemPlace() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProblemPlace that = (ProblemPlace) o;

        if (id != that.id) return false;
        if (place != null ? !place.equals(that.place) : that.place != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (place != null ? place.hashCode() : 0);
        return result;
    }
}
