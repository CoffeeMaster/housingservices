package il.co.hs4you.model.entity;

import javax.persistence.*;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 01.10.16.
 */

@Entity
@Table(name = "problem_type")
//TODO check for json overhead
public class ProblemType {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "problem_name", nullable = true, length = 45)
    private String problemName;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "problem_place_id")
    private ProblemPlace problemPlace;

    public ProblemType(String problemName, ProblemPlace problemPlace) {
        this.problemName = problemName;
        this.problemPlace = problemPlace;
    }

    public ProblemType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public ProblemPlace getProblemPlace() {
        return problemPlace;
    }

    public void setProblemPlace(ProblemPlace problemPlace) {
        this.problemPlace = problemPlace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProblemType that = (ProblemType) o;

        if (id != that.id) return false;
        if (problemName != null ? !problemName.equals(that.problemName) : that.problemName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (problemName != null ? problemName.hashCode() : 0);
        return result;
    }
}
