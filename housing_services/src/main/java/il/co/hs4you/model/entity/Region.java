package il.co.hs4you.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @version  1.0.0-SNAPSHOT
 * @since 30.09.16.
 */

@Entity
@Table(name="region")
public class Region implements Serializable{
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "region_name")
    private String regionName;

    @OneToMany(mappedBy = "region", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Address> address;

    public Region(){}

    public Region(String regionName) {
        this.regionName = regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public int getId() {
        return id;
    }

    public String getRegionName() {
        return regionName;
    }
}
