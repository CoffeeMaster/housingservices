package il.co.hs4you.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.1-SNAPSHOT
 * @since 01.10.16.
 */

@Entity
@Table(name="renter")
public class Renter {

    @Id
    @Column(name = "national_id", nullable = false)
    private int nationalId;

    @Basic
    @Column(name = "name", nullable = true, length = 40)
    private String name;

    @Basic
    @Column(name = "surname", nullable = true, length = 40)
    private String surname;

    @Basic
    @Column(name = "phone", nullable = true, length = 40)
    private String phone;

    @Basic
    @Column(name = "email", nullable = true, length = 45)
    private String email;

    @Basic
    @Column(name = "validated", nullable = false, columnDefinition = "boolean default false")
    private boolean validated;

    @Basic
    @Column(name = "token")
    private String token;

    @JsonIgnore
    @Basic
    @Column(name = "password", nullable = false, length = 60)
    private String password;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "flat_id")
    private Flat flat;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "renter_roles",
            joinColumns = {@JoinColumn(name = "renter_national_id")},
            inverseJoinColumns = {@JoinColumn(name="roles_id")}
    )
    private Set<Role> roles;

    public Renter(){}

    public Renter(Renter another){
        this.nationalId = another.getNationalId();
        this.name = another.getName();
        this.surname = another.getSurname();
        this.phone = another.getPhone();
        this.email = another.getEmail();
        this.validated = another.isValidated();
        this.token = another.getToken();
        this.password = another.getPassword();
        this.flat = another.getFlat();
    }

    public Renter(
            int nationalId,
            String name,
            String surname,
            String phone,
            String email,
            Flat flat
    ){
        this.nationalId = nationalId;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
        this.flat = flat;
    }

    public int getNationalId() {
        return nationalId;
    }

    public void setNationalId(int nationalId) {
        this.nationalId = nationalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Flat getFlat() {
        return flat;
    }

    public void setFlat(Flat flat) {
        this.flat = flat;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public ArrayList<String> roles(){
        ArrayList<String> roleNames = new ArrayList<>();
        Set<Role> roles = getRoles();
        for(Role role : roles)
            roleNames.add(role.getRoleName());
        return roleNames;
    }

    public void setRoles(String[] roles) {
        Set<Role> roleSet = new HashSet<>();
        for(String role : roles)
            roleSet.add(new Role(role));
        this.roles = roleSet;
    }

    public void addRole(Role role){
        if(roles != null)
            this.roles.add(role);
        else {
            this.roles = new HashSet<>();
            roles.add(role);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Renter renter = (Renter) o;

        if (nationalId != renter.nationalId) return false;
        if (name != null ? !name.equals(renter.name) : renter.name != null) return false;
        if (surname != null ? !surname.equals(renter.surname) : renter.surname != null) return false;
        if (phone != null ? !phone.equals(renter.phone) : renter.phone != null) return false;
        if (email != null ? !email.equals(renter.email) : renter.email != null) return false;
        if (password != null ? !password.equals(renter.password) : renter.password != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nationalId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Renter{" +
                "nationalId=" + nationalId +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", validated=" + validated +
                ", token='" + token + '\'' +
                ", password='" + password + '\'' +
                ", flat=" + flat +
                '}';
    }
}
