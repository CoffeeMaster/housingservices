package il.co.hs4you.model.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Matvey Mitnitsky
 * @since  11/15/16.
 */

@Entity
@Table(name = "roles")
public class Role {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "role_name", unique = true, nullable = false)
    private String roleName;

    @ManyToMany(mappedBy = "roles", cascade = CascadeType.ALL)
    private Set<Renter> renters;

    public Role(){

    }

    public Role(String roleName) {
        this.roleName = roleName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Set<Renter> getRenters() {
        return renters;
    }

    public void setRenters(Set<Renter> renters) {
        this.renters = renters;
    }
}
