package il.co.hs4you.model.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 01.10.16.
 */

@Entity
@Table(name = "squad")
public class Squad {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "town", nullable = true, length = 45)
    private String town;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable( name = "worker_squad",
            joinColumns = {@JoinColumn(name = "squad_id")},
            inverseJoinColumns = {@JoinColumn(name = "worker_national_id")}
    )
    private Set<Worker> workers;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public Set<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(Set<Worker> workers) {
        this.workers = workers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Squad squad = (Squad) o;

        if (id != squad.id) return false;
        if (town != null ? !town.equals(squad.town) : squad.town != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (town != null ? town.hashCode() : 0);
        return result;
    }
}
