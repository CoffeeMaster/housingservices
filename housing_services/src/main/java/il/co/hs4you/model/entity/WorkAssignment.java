package il.co.hs4you.model.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 01.10.16.
 */

@Entity
@Table(name = "work_assignment")
public class WorkAssignment {

    @Id
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "assigned_at", nullable = true)
    private Timestamp assignedAt;

    @Basic
    @Column(name = "assigned_on", nullable = true)
    private Timestamp assignedOn;

    @Basic
    @Column(name = "work_started", nullable = true)
    private Timestamp workStarted;

    @Basic
    @Column(name = "work_finished", nullable = true)
    private Timestamp workFinished;

    @Basic
    @Column(name = "work_status", nullable = true)
    @Enumerated(EnumType.STRING)
    private WorkStatus workStatus;

    @OneToOne
    @JoinColumn(name = "application_id")
    private Application application;

    @ManyToOne
    @JoinColumn(name = "squad_id")
    private Squad squad;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(Timestamp assignedAt) {
        this.assignedAt = assignedAt;
    }

    public Timestamp getAssignedOn() {
        return assignedOn;
    }

    public void setAssignedOn(Timestamp assignedOn) {
        this.assignedOn = assignedOn;
    }

    public Timestamp getWorkStarted() {
        return workStarted;
    }

    public void setWorkStarted(Timestamp workStarted) {
        this.workStarted = workStarted;
    }

    public Timestamp getWorkFinished() {
        return workFinished;
    }

    public void setWorkFinished(Timestamp workFinished) {
        this.workFinished = workFinished;
    }

    public WorkStatus getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(WorkStatus workStatus) {
        this.workStatus = workStatus;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public Squad getSquad() {
        return squad;
    }

    public void setSquad(Squad squad) {
        this.squad = squad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkAssignment that = (WorkAssignment) o;

        if (id != that.id) return false;
        if (assignedAt != null ? !assignedAt.equals(that.assignedAt) : that.assignedAt != null) return false;
        if (assignedOn != null ? !assignedOn.equals(that.assignedOn) : that.assignedOn != null) return false;
        if (workStarted != null ? !workStarted.equals(that.workStarted) : that.workStarted != null) return false;
        if (workFinished != null ? !workFinished.equals(that.workFinished) : that.workFinished != null) return false;
        if (workStatus != null ? !workStatus.equals(that.workStatus) : that.workStatus != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (assignedAt != null ? assignedAt.hashCode() : 0);
        result = 31 * result + (assignedOn != null ? assignedOn.hashCode() : 0);
        result = 31 * result + (workStarted != null ? workStarted.hashCode() : 0);
        result = 31 * result + (workFinished != null ? workFinished.hashCode() : 0);
        result = 31 * result + (workStatus != null ? workStatus.hashCode() : 0);
        return result;
    }
}
