package il.co.hs4you.model.entity;

/**
 * @author Matvey Mitnitsky
 * @since 02.11.16.
 */

public enum WorkStatus {
    newAssign,
    in_proccess,
    completed,
    reassigned
}
