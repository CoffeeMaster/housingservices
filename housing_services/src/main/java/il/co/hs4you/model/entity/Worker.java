package il.co.hs4you.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Matvey Mitnitsky
 * @since  11/28/16.
 */

@Entity
public class Worker extends Renter{

    @Basic
    @Column(name = "salary", nullable = true, precision = 0)
    private Double salary;

    @Basic
    @Column(name = "work_types", nullable = true, length = 45)
    private String workTypes;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "worker_squad",
            joinColumns = {@JoinColumn(name = "worker_national_id")},
            inverseJoinColumns = {@JoinColumn(name="squad_id")})
    @JsonIgnore
    private Set<Squad> squads;
    
    public Worker(){}
    
    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getWorkTypes() {
        return workTypes;
    }

    public void setWorkTypes(String workTypes) {
        this.workTypes = workTypes;
    }

    public Set<Squad> getSquads() {
        return squads;
    }

    public void setSquads(Set<Squad> squads) {
        this.squads = squads;
    }
}
