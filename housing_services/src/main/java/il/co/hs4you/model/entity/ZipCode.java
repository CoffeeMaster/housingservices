package il.co.hs4you.model.entity;

import javax.persistence.*;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 01.10.16.
 */

@Entity
@Table(name = "zip_code")
public class ZipCode {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "zip_code", nullable = false)
    private int zipCode;

    @Basic
    @Column(name = "town", nullable = true, length = 45)
    private String town;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ZipCode zipCode1 = (ZipCode) o;

        if (id != zipCode1.id) return false;
        if (zipCode != zipCode1.zipCode) return false;
        if (town != null ? !town.equals(zipCode1.town) : zipCode1.town != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + zipCode;
        result = 31 * result + (town != null ? town.hashCode() : 0);
        return result;
    }
}
