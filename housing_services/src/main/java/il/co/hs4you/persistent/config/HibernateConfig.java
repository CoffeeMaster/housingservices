package il.co.hs4you.persistent.config;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.1-SNAPSHOT
 * @since 08.10.16.
 * 
 */

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:hibernate-mysql.properties")
public class HibernateConfig {

    @Autowired
    Environment env;

    @Bean
    public DataSource dataSource(){
        BasicDataSource ds = new BasicDataSource();

        ds.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        ds.setUrl(env.getProperty("jdbc.url"));
        ds.setUsername(env.getProperty("jdbc.user"));
        ds.setPassword(env.getProperty("jdbc.pass"));
        ds.setInitialSize(
                Integer.parseInt(env.getProperty("jdbc.connection.pool_size"))
        );
        ds.setMaxActive(
                Integer.parseInt(env.getProperty("jdbc.maxActive"))
        );
        ds.setMaxIdle(
                Integer.parseInt(env.getProperty("jdbc.maxIdle"))
        );
        ds.setMaxWait(
                Long.parseLong(env.getProperty("jdbc.maxWait"))
        );
        return ds;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory(){
        LocalSessionFactoryBean sessionFactoryBean =
                new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource());
        sessionFactoryBean.setPackagesToScan(
                "il.co.hs4you.persistent",
                "il.co.hs4you.model.entity"
        );
        sessionFactoryBean.setHibernateProperties(hibernateProperties());
        return sessionFactoryBean;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory){
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);
        return txManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    Properties hibernateProperties(){
        Properties properties = new Properties();
        properties.setProperty(
                "hibernate.dialect",
                env.getProperty("hibernate.dialect")
        );

        properties.setProperty(
                "hibernate.hbm2ddl.auto",
                env.getProperty("hibernate.hbm2ddl.auto")
        );
        properties.setProperty(
                "hibernate.show_sql",
                env.getProperty("hibernate.show_sql")
        );

        return properties;
    }
}
