package il.co.hs4you.persistent.dao;

import il.co.hs4you.model.entity.Address;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 01.10.16..
 */

public interface AddressDAO extends IDAO<Address> {
    Address findByRestrictions(String town, String street, String houseNumber);
}
