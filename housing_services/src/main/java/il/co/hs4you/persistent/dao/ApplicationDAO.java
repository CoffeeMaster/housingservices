package il.co.hs4you.persistent.dao;

import il.co.hs4you.model.entity.Application;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @since  11/12/16.
 */

public interface ApplicationDAO extends IDAO<Application>{

    List<Application> findAllByNationalId(int nationalId);

}
