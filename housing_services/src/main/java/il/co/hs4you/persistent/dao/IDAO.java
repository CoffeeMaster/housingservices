package il.co.hs4you.persistent.dao;

import java.io.Serializable;
import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @since  28.10.16.
 */

public interface IDAO<T> {
    T readById(int id);
    List<T> readAll();
    void create(T obj);
    void update(T obj);
}
