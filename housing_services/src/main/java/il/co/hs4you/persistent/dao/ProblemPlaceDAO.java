package il.co.hs4you.persistent.dao;

import il.co.hs4you.model.entity.ProblemPlace;

/**
 * @author  Matvey Mitnitsky
 * @since  11/12/16.
 */

public interface ProblemPlaceDAO{
    ProblemPlace findByPlace(String place);
}
