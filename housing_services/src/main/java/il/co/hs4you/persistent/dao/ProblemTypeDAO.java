package il.co.hs4you.persistent.dao;

import il.co.hs4you.model.entity.ProblemType;

/**
 * @author Matvey Mitnitsky
 * @since  11/12/16.
 */

public interface ProblemTypeDAO {
    ProblemType findByName(String problemName);
    ProblemType findByNameAndPlace(String problemName, String place);
}
