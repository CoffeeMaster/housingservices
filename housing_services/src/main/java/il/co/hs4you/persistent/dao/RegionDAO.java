package il.co.hs4you.persistent.dao;

import il.co.hs4you.model.entity.Region;

import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 30.09.16.
 */
public interface RegionDAO extends IDAO<Region> {
    Region findRegionByName(String regionName);
}
