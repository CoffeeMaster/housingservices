package il.co.hs4you.persistent.dao;

import il.co.hs4you.model.entity.Renter;
import il.co.hs4you.model.entity.Worker;

import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.1-SNAPSHOT
 * @since 10.10.16.
 */

public interface RenterDAO extends IDAO<Renter> {
    Renter readByEmail(String email);

    List<Renter> findAllByRoles(String [] roles);

    boolean addRole(int nationalID, String roleName);
    
    boolean addWorkerRole(int nationalID);

    Worker findWorkerById(int nationalID);

    List<Worker> findAllWorkers();
}
