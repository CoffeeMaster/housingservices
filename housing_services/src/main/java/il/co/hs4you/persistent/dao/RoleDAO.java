package il.co.hs4you.persistent.dao;

import il.co.hs4you.model.entity.Role;

/**
 * @author Matvey Mitnitsky
 * @since  11/16/16.
 */

public interface RoleDAO {
    Role findByName(String roleName);
}
