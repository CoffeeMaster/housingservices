package il.co.hs4you.persistent.dao.impl;

import il.co.hs4you.model.entity.Address;
import il.co.hs4you.persistent.dao.AddressDAO;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.internal.CriteriaImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 01.10.16.
 */


@Repository
public class AddressDAOImpl implements AddressDAO {

    SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    private Session getSession(){
        return this.sessionFactory.openSession();
    }

    @Override
    public Address readById(int id) {
        Session session = getSession();
        Address address = (Address) session.get(Address.class, id);
        session.close();
        return address;
    }

    @Override
    public List<Address> readAll() {
        return null;
    }

    @Override
    public void create(Address obj) {

    }

    @Override
    public void update(Address obj) {

    }

    @Override
    public Address findByRestrictions(String town, String street, String houseNumber) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Address.class)
                .add(Restrictions.eq("town", town))
                .add(Restrictions.eq("street", street))
                .add(Restrictions.eq("houseNumb", houseNumber))
                .setFetchMode("building.flats", FetchMode.JOIN);
        List<Address> addressList = criteria.list();
        if(addressList.isEmpty()) {
            session.close();
            return null;
        }
        Address address = addressList.get(0);
        session.close();
        return address;
    }
}
