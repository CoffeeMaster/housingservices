package il.co.hs4you.persistent.dao.impl;

import il.co.hs4you.model.entity.Application;
import il.co.hs4you.persistent.dao.ApplicationDAO;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @since  11/12/16.
 */

@Transactional
@Repository
public class ApplicationDAOImpl implements ApplicationDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Application readById(int id) {
        Session session = sessionFactory.openSession();
        Application application = (Application) session.get(Application.class, id);
        session.close();
        return application;
    }

    @Override
    public List<Application> findAllByNationalId(int nationalId) {
        Session session = sessionFactory.openSession();
        List<?> applications = session.createCriteria(Application.class, "application")
                .createAlias("application.renter", "renter")
                .add(Restrictions.eq("renter.nationalId", nationalId))
                .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                .list();
        if(applications == null || applications.isEmpty()){
            session.close();
            return null;
        }
        session.close();
        return (List<Application>) applications;
    }

    @Override
    public List<Application> readAll() {
        Session session = sessionFactory.openSession();
        List<?> applications = session.createQuery("FROM Application").list();
        session.close();
        return (List<Application>) applications;
    }

    @Override
    public void create(Application application) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.save(application);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Application application) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.update(application);
            tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            tx.rollback();
        } finally {
            session.close();
        }
    }
}
