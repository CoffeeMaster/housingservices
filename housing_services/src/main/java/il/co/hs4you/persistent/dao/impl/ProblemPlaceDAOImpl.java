package il.co.hs4you.persistent.dao.impl;

import il.co.hs4you.model.entity.ProblemPlace;
import il.co.hs4you.persistent.dao.ProblemPlaceDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @since  11/12/16.
 */

@Transactional
@Repository
public class ProblemPlaceDAOImpl implements ProblemPlaceDAO{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ProblemPlace findByPlace(String place) {
        Session session = sessionFactory.openSession();
        List places = session.createCriteria(ProblemPlace.class)
                .add(Restrictions.eq("place", place))
                .list();
        if(places.isEmpty()){
            session.close();
            return null;
        }
        session.close();
        return (ProblemPlace) places.get(0);
    }
}
