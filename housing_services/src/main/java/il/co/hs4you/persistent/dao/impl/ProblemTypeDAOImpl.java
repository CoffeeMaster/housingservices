package il.co.hs4you.persistent.dao.impl;

import il.co.hs4you.model.entity.ProblemType;
import il.co.hs4you.persistent.dao.ProblemTypeDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @since  11/12/16.
 */

@Transactional
@Repository
public class ProblemTypeDAOImpl implements ProblemTypeDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ProblemType findByName(String problemName) {
        Session session = sessionFactory.openSession();
        List problems = session.createCriteria(ProblemType.class)
                .add(Restrictions.eq("problemName", problemName))
                .list();
        if(problems.isEmpty()){
            session.close();
            return null;
        }
        session.close();
        return (ProblemType) problems.get(0);
    }

    @Override
    public ProblemType findByNameAndPlace(String problemName, String place) {
        Session session = sessionFactory.openSession();
        List problems = session.createCriteria(ProblemType.class, "typeOfProblem")
                .createAlias("typeOfProblem.problemPlace", "probPlace")
                .add(Restrictions.eq("problemName", problemName))
                .add(Restrictions.eq("probPlace.place", place))
                .list();
        if(problems.isEmpty()){
            session.close();
            return null;
        }
        session.close();
        return (ProblemType) problems.get(0);
    }
}
