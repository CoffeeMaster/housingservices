package il.co.hs4you.persistent.dao.impl;

import il.co.hs4you.model.entity.Region;
import il.co.hs4you.persistent.dao.RegionDAO;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.1-SNAPSHOT
 * @since 30.09.16.
 */

@Transactional
@Repository
public class RegionDAOImpl implements RegionDAO{

    @Autowired
    SessionFactory sessionFactory;

    private Session getSession(){
        return this.sessionFactory.openSession();
    }

    /*
    * CRU methods for Region Entity;
    */

    @Override
    public Region readById(int id) {
        Session session = getSession();
        Region region = (Region) session.get(Region.class, id);
        session.close();
        return region;
    }

    @Override
    public void create(Region region) {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        try {
            session.save(region);
            tx.commit();
        } catch (Exception ex){
            ex.printStackTrace();
            tx.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Region region) {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        try {
            session.update(region);
            tx.commit();
        } catch (Exception ex){
            ex.printStackTrace();
            tx.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public Region findRegionByName(String regionName) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Region.class)
                .add(Restrictions.eq("regionName", regionName))
                .setMaxResults(1);
        List<Region> regionList = criteria.list();
        if(regionList.isEmpty()){
            session.close();
            return null;
        }
        session.close();
        return regionList.get(0);
    }

    @Override
    public List<Region> readAll() {
        Session session = getSession();
        Query query = session.createQuery("From Region");
        List<Region> regionList = query.list();
        session.close();
        return regionList;
    }
}
