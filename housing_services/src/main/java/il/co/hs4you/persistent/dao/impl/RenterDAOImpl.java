package il.co.hs4you.persistent.dao.impl;

import il.co.hs4you.model.entity.Renter;
import il.co.hs4you.model.entity.Role;
import il.co.hs4you.model.entity.Worker;
import il.co.hs4you.persistent.dao.RenterDAO;
import il.co.hs4you.persistent.dao.RoleDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 10.10.16.
 */

@Transactional
@Repository
public class RenterDAOImpl implements RenterDAO{

    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    RoleDAO roleDAO;

    @Override
    public Renter readById(int nationalId) {
        Session session = sessionFactory.openSession();
        Renter renter = (Renter) session.get(Renter.class, nationalId);
        session.close();
        return renter;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Renter readByEmail(String email) {
        Session session = sessionFactory.openSession();
        List<Renter> renters = session.createCriteria(Renter.class)
                .add(Restrictions.like("email", email))
                .list();
        if(renters.isEmpty()){
            session.close();
            return null;
        }
        session.close();
        return renters.get(0);
    }

    @Override
    public void create(Renter renter) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.save(renter);
            tx.commit();
        } catch (Exception ex) {
            //TODO write to log
            ex.printStackTrace();
            tx.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Renter renter) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.update(renter);
            tx.commit();
        } catch (Exception ex){
            ex.printStackTrace();
            tx.rollback();
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("JpaQlInspection")
    @Override
    public List<Renter> readAll() {
        List<Renter> renters = null;
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("From Renter");
        renters = query.list();
        session.close();
        return renters;
    }

    @Override
    public List<Renter> findAllByRoles(String [] roles) {
        Session session = sessionFactory.openSession();
        List<?> renters = session.createCriteria(Renter.class, "renter")
                .createAlias("renter.roles", "roles")
                .add(Restrictions.in("roles.roleName", roles))
                .list();
        return (List<Renter>) renters;
    }

    @Override
    public boolean addRole(int nationalID, String roleName) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            Role role = roleDAO.findByName(roleName);
            Renter renter = readById(nationalID);
            if(role == null || renter == null)
                return false;
            renter.addRole(role);
            session.update(renter);
            tx.commit();
        } catch (Exception ex){
            tx.rollback();
            //TODO log
            ex.printStackTrace();
            return false;
        } finally {
            session.close();
        }
        return true;
    }
    
    @Override
    public boolean addWorkerRole(int nationalID) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            Role role = roleDAO.findByName("WORKER");
            Renter renter = readById(nationalID);
            if(role == null || renter == null)
                return false;
            renter.addRole(role);
            session.update(renter);
            String query = String.format("Update Renter SET DTYPE = 'Worker' where nationalId = %d", nationalID);
            session.createQuery(query).executeUpdate();
            tx.commit();
        } catch (Exception ex){
            tx.rollback();
            //TODO log
            ex.printStackTrace();
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public Worker findWorkerById(int nationalID) {
        Session session = sessionFactory.openSession();
        Worker worker = (Worker) session.get(Worker.class, nationalID);
        session.close();
        return worker;
    }

    @Override
    public List<Worker> findAllWorkers() {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("SELECT w FROM Worker w");
        List<Worker> workers = query.list();
        session.close();
        return workers;
    }
}
