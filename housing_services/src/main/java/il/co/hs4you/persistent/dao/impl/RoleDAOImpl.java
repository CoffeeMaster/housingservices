package il.co.hs4you.persistent.dao.impl;

import il.co.hs4you.model.entity.Role;
import il.co.hs4you.persistent.dao.RoleDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @since  11/16/16.
 */

@Repository
@Transactional
public class RoleDAOImpl implements RoleDAO {
    @Autowired
    SessionFactory sessionFactory;

    @Override
    public Role findByName(String roleName) {
        Session session = sessionFactory.openSession();
        List roles = session.createCriteria(Role.class)
                .add(Restrictions.eq("roleName", roleName))
                .list();
        if(roles.isEmpty()){
            session.close();
            return null;
        }
        session.close();
        return (Role) roles.get(0);
    }
}
