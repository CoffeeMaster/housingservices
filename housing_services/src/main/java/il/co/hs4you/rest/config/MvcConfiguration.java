package il.co.hs4you.rest.config;

import il.co.hs4you.persistent.config.HibernateConfig;
//import il.co.hs4you.security.SpringSecurityConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 24.09.16.
 * resolves view configuration, enabling spring-mvc
 */

@Configuration
@Import({HibernateConfig.class})
public class MvcConfiguration extends WebMvcConfigurerAdapter {
}
