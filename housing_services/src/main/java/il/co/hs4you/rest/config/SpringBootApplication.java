package il.co.hs4you.rest.config;

import il.co.hs4you.persistent.config.HibernateConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 23.09.16.
 */

@org.springframework.boot.autoconfigure.SpringBootApplication(
        scanBasePackages = {
                "il.co.hs4you.rest.controllers",
                "il.co.hs4you.config",
                "il.co.hs4you.persistent.dao",
                "il.co.hs4you.service",
                "il.co.hs4you.model.entity",
                "il.co.hs4you.security"
        })
@Import({MvcConfiguration.class, HibernateConfig.class})
public class SpringBootApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringBootApplication.class);
    }
}
