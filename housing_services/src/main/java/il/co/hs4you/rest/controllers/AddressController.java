package il.co.hs4you.rest.controllers;

import il.co.hs4you.model.entity.Address;
import il.co.hs4you.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *@author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since  01.10.16.
 */

@RestController
public class AddressController {

    @Autowired
    AddressService addressService;

    @RequestMapping(value = "/address",
                    method = RequestMethod.GET,
                    produces = "application/json")
    public Address getAddress(@RequestParam(defaultValue = "1") int id){
        return addressService.getAddressById(id);
    }
}
