package il.co.hs4you.rest.controllers;

import il.co.hs4you.model.entity.Application;
import il.co.hs4you.rest.support.JsonApplicationsList;
import il.co.hs4you.rest.support.JsonResponse;
import il.co.hs4you.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import static org.springframework.web.bind.annotation.RequestMethod.*;


/**
 * @author Matvey Mitnitsky
 * @since  11/12/16.
 */

@Controller
@RequestMapping(value = "/application")
public class ApplicationController {
    
    @Autowired
    ApplicationService applicationService;

    @RequestMapping(value = "/send", method = POST)
    @ResponseBody
    public JsonResponse createApplication(
            @RequestParam String title,
            @RequestParam String problemName,
            @RequestParam String problemPlace,
            @RequestParam String description,
            Authentication authentication
    ){
        Integer nationalID = Integer.valueOf(authentication.getName());
        return applicationService.sendApplication(
                nationalID,
                title,
                problemName,
                problemPlace,
                description
        );
    }

    @RequestMapping(method = GET)
    @ResponseBody
    public Application displayApplication(
            @RequestParam int id
    ){
        return applicationService.findApplicationById(id);
    }

    @RequestMapping(value = "/all", method = GET)
    @ResponseBody
    public JsonApplicationsList displayAllRentersApplications(
            Authentication authentication
    ){
        Integer nationalID = Integer.valueOf(authentication.getName());
        return applicationService.findAllApplications(nationalID);
    }
    
    @RequestMapping(value = "/edit", method = POST)
    @ResponseBody
    public JsonResponse editApplication(
            @RequestParam int applicationId,
            @RequestParam String title,
            @RequestParam String problemName,
            @RequestParam String problemPlace,
            @RequestParam String description,
            Authentication authentication
    ) {
        Integer nationalID = Integer.valueOf(authentication.getName());
        return applicationService.editApplication(
                applicationId,
                title,
                problemName,
                problemPlace,
                description,
                nationalID
        );
    }

    @RequestMapping(value = "/create", method = GET)
    public ModelAndView toApplicationPage() {
        return new ModelAndView("redirect:/app.html");
    }

    @RequestMapping(value = "/admin/all", method = GET)
    @ResponseBody
    public JsonApplicationsList displayAllSentApplications(
    ){
        return applicationService.findAllApplications();
    }

    @RequestMapping(value = "/admin/all/paging", method = GET)
    @ResponseBody
    public JsonApplicationsList displayAllSentApplications(
            @RequestParam (defaultValue = "10") int resultsPerPage,
            @RequestParam (defaultValue = "0") int pageNumber
    ){
        //TODO create Service logic and handle request
        return new JsonApplicationsList(false, 404, "STUB");
    }
    
}