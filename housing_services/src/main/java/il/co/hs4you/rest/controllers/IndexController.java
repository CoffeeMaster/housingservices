package il.co.hs4you.rest.controllers;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 23.09.16.
 */

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    /*
     * @method dispatching http-request mapped on "/",
     * @return welcome page view, which is index.html
     */
    @RequestMapping(value = "/")
    public String index() {
        return "index.html";
    }

    /*
     * simple rest test @method
     */
    @RequestMapping(value="/test", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String test(){
        return "{ \"test\" : \"ok\" }";
    }

}
