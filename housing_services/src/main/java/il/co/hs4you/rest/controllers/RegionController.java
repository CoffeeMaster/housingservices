package il.co.hs4you.rest.controllers;

import il.co.hs4you.model.entity.Region;
import il.co.hs4you.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 30.09.16.
 */

@RestController
public class RegionController {

    RegionService regionService;

    @Autowired
    public void setRegionService(RegionService service){
        this.regionService = service;
    }

    @RequestMapping(value = "/regions",
            method = RequestMethod.GET,
            produces = "application/json")
    public Region getRegion(@RequestParam(defaultValue = "1") int id ){
        return regionService.getRegionById(id);
    }

    @RequestMapping(value = "/regions/all",
                    method = RequestMethod.GET,
                    produces = "application/json"
    )
    public List<Region> getAllRegions(){
        return regionService.selectAll();
    }

    @RequestMapping(value = "/regions/test",
            method = RequestMethod.GET,
            produces = "application/json")
    public String test(){
        return "{ \"test\" : \"ok\" }";
    }
}
