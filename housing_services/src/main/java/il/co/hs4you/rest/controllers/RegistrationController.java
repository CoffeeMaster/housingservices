package il.co.hs4you.rest.controllers;

import il.co.hs4you.rest.support.JsonResponse;
import il.co.hs4you.service.RenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by matvey on 01.11.16.
 */

@Controller
@RequestMapping("/renter")
public class RegistrationController {

    @Autowired
    RenterService renterService;

    @RequestMapping(value = "/reg2", method = POST)
    @ResponseBody
    public JsonResponse extendedRegistration(
            @RequestParam String region,
            @RequestParam String town,
            @RequestParam String street,
            @RequestParam String houseNumber,
            @RequestParam int flatNumber,
            Authentication authentication
    ){
        Integer nationalID = Integer.valueOf(authentication.getName());
        return renterService.updateRenterProfile(
                nationalID,
                region,
                town,
                street,
                houseNumber,
                flatNumber
        );
    }
}
