package il.co.hs4you.rest.controllers;

import il.co.hs4you.model.entity.Renter;
import il.co.hs4you.rest.support.JsonResponse;
import il.co.hs4you.service.RenterService;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.1-SNAPSHOT
 * @since 10.10.16.
 */

@Controller
@RequestMapping("/renter")
public class RenterController {

    @Autowired
    RenterService renterService;

    @Autowired
    AuthenticationManager authManager;

    @Autowired
    UserDetailsService userDetailsService;

    @RequestMapping(value = "/signup", method = POST)
    public ModelAndView signUp(
            @RequestParam(defaultValue = "0") int nationalID,
            @RequestParam String name,
            @RequestParam String surname,
            @RequestParam String password,
            @RequestParam String confirmPassword,
            @RequestParam String email,
            @RequestParam String phone
    ){

        if(password.equals(confirmPassword)){
            if(renterService.register(nationalID, name, surname, password, email, phone)) {
                UserDetails userDetails =
                        userDetailsService.loadUserByUsername(String.valueOf(nationalID));
                UsernamePasswordAuthenticationToken token =
                        new UsernamePasswordAuthenticationToken(
                                userDetails,
                                password,
                                userDetails.getAuthorities()
                        );
                authManager.authenticate(token);

                if (token.isAuthenticated()) {
                    SecurityContextHolder.getContext().setAuthentication(token);
                    return new ModelAndView("redirect:/profile.html");
                }
            }
            return new ModelAndView("redirect:/login_error.html");
        }
        return new ModelAndView("redirect:/login_error.html");
    }

    @RequestMapping(value = "/profile", method = GET)
    @ResponseBody
    public Renter getProfile(Authentication authentication){
        if(!(authentication instanceof AnonymousAuthenticationToken)){
            Integer nationalID = Integer.valueOf(authentication.getName());
            return renterService.getRenterById(nationalID);
        }
        return new Renter();
    }

    @RequestMapping(value = "/chpass", method = POST)
    @ResponseBody
    public JsonResponse changePassword(
            @RequestParam String oldPassword,
            @RequestParam String newPassword,
            @RequestParam String confPassword,
            Authentication authentication
    ){
        Integer nationalID = Integer.valueOf(authentication.getName());
        return renterService.changePassword(
                nationalID,
                oldPassword,
                newPassword,
                confPassword
        );
    }

    @RequestMapping(value = "/edit", method = POST)
    @ResponseBody
    public JsonResponse editPersonalInfo(
            @RequestParam String name,
            @RequestParam String surname,
            @RequestParam String phone,
            @RequestParam String email,
            Authentication authentication
    ){
        Integer nationalID = Integer.valueOf(authentication.getName());
        return renterService.editPersonalInfo(
                nationalID,
                name,
                surname,
                phone,
                email
        );
    }

    @RequestMapping(value = "/address/edit", method = POST)
    @ResponseBody
    public JsonResponse editAddressInfo(
            @RequestParam String region,
            @RequestParam String town,
            @RequestParam String street,
            @RequestParam String houseNumber,
            @RequestParam(defaultValue = "0") int flatNumber,
            Authentication authentication
    ){
        Integer nationalID = Integer.valueOf(authentication.getName());
        return renterService.updateRenterAddress(
                nationalID,
                region,
                town,
                street,
                houseNumber,
                flatNumber
        );
    }

    @RequestMapping(value = "/flat/edit", method = POST)
    @ResponseBody
    public JsonResponse editFlatInfo(
            @RequestParam(defaultValue = "0") int rooms,
            @RequestParam (defaultValue = "0") int balconies,
            @RequestParam (defaultValue = "0.0") double square,
            Authentication authentication
    ){
        Integer nationalID = Integer.valueOf(authentication.getName());
        return renterService.updateFlatInfo(
                nationalID,
                rooms,
                balconies,
                square
        );
    }
}