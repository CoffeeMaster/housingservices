package il.co.hs4you.rest.controllers;

import il.co.hs4you.model.entity.Renter;
import il.co.hs4you.model.entity.Worker;
import il.co.hs4you.rest.support.JsonListRespWrapper;
import il.co.hs4you.rest.support.JsonRespWrapper;
import il.co.hs4you.rest.support.JsonResponse;
import il.co.hs4you.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * @author Matvey Mitnitsky
 * @since  11/28/16.
 */

@Controller
@RequestMapping("/worker")
public class WorkerController {
    
    @Autowired
    WorkerService workerService;
    
    @RequestMapping(value = "/all", method = GET)
    @ResponseBody
    public JsonListRespWrapper<Worker> findAllWorkers(){
        return workerService.findAllWorkers(); 
    }
    
    @RequestMapping(method = GET)
    @ResponseBody
    public JsonRespWrapper<Worker> findWorkerById(
            @RequestParam int nationalID
    ){
        return workerService.findWorkerById(nationalID);
    }
    
    @RequestMapping(value = "/add", method = POST)
    @ResponseBody
    public JsonResponse addWorker(
            @RequestParam int nationalID
    ){
        return workerService.addWorker(nationalID);
    }
}
