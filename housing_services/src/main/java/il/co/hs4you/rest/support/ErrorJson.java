package il.co.hs4you.rest.support;

import java.util.Map;

/**
 * @author Matvey Mitnitsky
 * copied from
 * <a href="https://gist.github.com/jonikarppinen/662c38fb57a23de61c8b">Handling Errors Sample</a>
 * @since  03.11.16.
 */

public class ErrorJson {

    public Integer status;
    public String error;
    public String message;
    public String timeStamp;
    public String trace;

    public ErrorJson(int status, Map<String, Object> errorAttributes) {
        this.status = status;
        this.error = (String) errorAttributes.get("error");
        this.message = (String) errorAttributes.get("message");
        this.timeStamp = errorAttributes.get("timestamp").toString();
        this.trace = (String) errorAttributes.get("trace");
    }

}