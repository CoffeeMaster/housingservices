package il.co.hs4you.rest.support;

import il.co.hs4you.model.entity.Application;

import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @since  11/12/16.
 */

public class JsonApplicationsList {
    public boolean success;
    public Integer status;
    public String reason;
    public List<Application> applications;

    public JsonApplicationsList(boolean success) {
        this.success = success;
    }

    public JsonApplicationsList(boolean success, Integer status) {
        this.success = success;
        this.status = status;
    }

    public JsonApplicationsList(boolean success, Integer status, String reason) {
        this.success = success;
        this.status = status;
        this.reason = reason;
    }

    public JsonApplicationsList(boolean success, Integer status, List<Application> applications) {
        this.success = success;
        this.status = status;
        this.applications = applications;
    }
}
