package il.co.hs4you.rest.support;

import java.util.List;

/**
 * Created by matvey on 11/28/16.
 */
public class JsonListRespWrapper<T> {
    public boolean success;
    public String reason;
    public List<T> result;

    public JsonListRespWrapper(boolean success) {
        this.success = success;
    }

    public JsonListRespWrapper(boolean success, String reason) {
        this.success = success;
        this.reason = reason;
    }

    public JsonListRespWrapper(boolean success, List<T> result) {
        this.success = success;
        this.result = result;
    }
}
