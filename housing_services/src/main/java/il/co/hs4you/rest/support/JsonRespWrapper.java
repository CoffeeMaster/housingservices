package il.co.hs4you.rest.support;


/**
 * @author Matvey Mitnitsky
 * @since on 11/28/16.
 */

public class JsonRespWrapper<T> {
    public boolean success;
    public String reason;
    public T result;

    public JsonRespWrapper(boolean success){
        this.success = success;
    }

    public JsonRespWrapper(boolean success, String reason){
        this.success = success;
        this.reason = reason;
    }

    public JsonRespWrapper(boolean success, T result) {
        this.success = success;
        this.result = result;
    }
}
