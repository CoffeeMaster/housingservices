package il.co.hs4you.rest.support;

/**
 * @author Matvey Mitnitsky
 * @since  07.11.16.
 */

public class JsonResponse {
    public boolean success;
    public String reason;

    public JsonResponse(boolean success){
        this.success = success;
    }

    public JsonResponse(boolean success, String reason){
        this.success = success;
        this.reason = reason;
    }
}
