package il.co.hs4you.security;

import il.co.hs4you.model.entity.Renter;
import il.co.hs4you.model.entity.Role;
import il.co.hs4you.persistent.dao.RenterDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Matvey Mitnitsky
 * @since 11/15/16.
 */

@Service
@Transactional
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    RenterDAO renterDAO;

    @Override
    public UserDetails loadUserByUsername(String nationalID) throws UsernameNotFoundException {
        //TODO set check on String input for nationalID... (regex)
        Renter renter = renterDAO.readById(Integer.valueOf(nationalID));
        if(renter == null)
            throw new UsernameNotFoundException("No user found with nationalID: " + nationalID);
        return new User(
                String.valueOf(renter.getNationalId()),
                renter.getPassword(),
                getRenterAuthorities(renter.getRoles())
        );
    }

    private List<GrantedAuthority> getRenterAuthorities(Set<Role> roles){
        List<GrantedAuthority> authorities = new ArrayList<>(roles.size());
        for(Role role : roles)
            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
        return authorities;
    }
}
