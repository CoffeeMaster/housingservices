package il.co.hs4you.security;

import il.co.hs4you.security.handlers.LoginFailureHandler;
import il.co.hs4you.security.handlers.LoginSuccessHandler;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * @author Matvey Mintitksy
 * @since  11/15/16.
 */

@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

    private static final String[] ADMIN_ACCESS_LEVEL = {
            "/address",
            "/application",
            "/application/admin/**",
            "/worker/**",
            "/regions/**",
            "/dump",
            "/mappings/**",
            "/health/**",
            "/beans/**"
    };


     @Override
     public void configure(AuthenticationManagerBuilder auth) throws Exception {
         auth.userDetailsService(myUserDetailsService());
         auth.authenticationProvider(authenticationProvider());
     }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                    .authorizeRequests()
                .mvcMatchers(ADMIN_ACCESS_LEVEL).hasAuthority("ADMIN")
                .mvcMatchers(
                        "/register.html",
                        "/renter/signup",
                        "/login_error.html").anonymous()
                .anyRequest().authenticated()
                    .and()
                .formLogin()
                .loginPage("/index.html")
                .loginProcessingUrl("/renter/login")
                .failureForwardUrl("/login?error")
                .successHandler(authenticationSuccessHandler())
                .failureHandler(authenticationFailureHandler())
                .permitAll()
                    .and()
                .logout().logoutUrl("/renter/logout").permitAll()
                    .and()
                .csrf().disable();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(myUserDetailsService());
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler(){
        return new LoginSuccessHandler();
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler(){
        return new LoginFailureHandler();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsService myUserDetailsService(){
        return new MyUserDetailsService();
    }
}
