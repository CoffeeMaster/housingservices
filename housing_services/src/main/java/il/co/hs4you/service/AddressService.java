package il.co.hs4you.service;

import il.co.hs4you.model.entity.Address;
import il.co.hs4you.persistent.dao.AddressDAO;
import il.co.hs4you.persistent.dao.impl.AddressDAOImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 01.10.16.
 */

@Service
public class AddressService {

    @Autowired
    private AddressDAO addressDAO;

    public Address getAddressById(int id){
        return addressDAO.readById(id);
    }
}
