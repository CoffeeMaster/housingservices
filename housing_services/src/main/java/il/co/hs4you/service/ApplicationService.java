package il.co.hs4you.service;

import il.co.hs4you.model.entity.*;
import il.co.hs4you.persistent.dao.ApplicationDAO;
import il.co.hs4you.persistent.dao.ProblemPlaceDAO;
import il.co.hs4you.persistent.dao.ProblemTypeDAO;
import il.co.hs4you.persistent.dao.RenterDAO;
import il.co.hs4you.rest.support.JsonApplicationsList;
import il.co.hs4you.rest.support.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @since  11/12/16.
 */

@Service
public class ApplicationService {

    @Autowired
    ApplicationDAO applicationDAO;

    @Autowired
    RenterDAO renterDAO;

    @Autowired
    ProblemPlaceDAO problemPlaceDAO;

    @Autowired
    ProblemTypeDAO problemTypeDAO;

    public JsonResponse sendApplication(int nationalId, String title, String problemName, String problemPlace, String description) {
        Renter currRenter = renterDAO.readById(nationalId);
        ProblemType problemType = defineProblemType(problemName, problemPlace);
        applicationDAO.create(
                new Application(
                    title,
                    description,
                    new Timestamp(new Date().getTime()),
                    ApplicationStatus.newAppl,
                    currRenter,
                    problemType
                )
        );
        return new JsonResponse(true);
    }

    private ProblemType defineProblemType(String problemName, String problemPlace) {
        ProblemType toP = problemTypeDAO.findByNameAndPlace(problemName, problemPlace);
        if(toP != null)
            return toP;

        ProblemPlace place = problemPlaceDAO.findByPlace(problemPlace);
        if(place != null)
            return new ProblemType(problemName, place);

        return new ProblemType(problemName, new ProblemPlace(problemPlace));
    }

    public Application findApplicationById(int id) {
        return applicationDAO.readById(id);
    }

    public JsonApplicationsList findAllApplications(int nationalId) {
        List<Application> applications = applicationDAO.findAllByNationalId(nationalId);
        if(applications == null || applications.isEmpty())
            return new JsonApplicationsList(false, 502, "failedToFindApplications");
        return new JsonApplicationsList(true, 200, applications);
    }

    public JsonResponse editApplication(
            int applicationId,
            String title,
            String problemName,
            String problemPlace,
            String description,
            int nationalId
    ) {
        Application applicationToUpdate = applicationDAO.readById(applicationId);
        if(applicationToUpdate == null)
            return new JsonResponse(false, "failedToRetrieveApplication");
        if(applicationToUpdate.getRenter().getNationalId() != nationalId)
            return new JsonResponse(false, "renterNotAllowed");

        if(title!=null && !title.equals(""))
            applicationToUpdate.setTitle(title);
        if(description!=null)
            applicationToUpdate.setDescription(description);

        if(problemName == null || problemName.equals(""))
            problemName = applicationToUpdate.getProblemType().getProblemName();
        if(problemPlace == null || problemName.equals(""))
            problemPlace = applicationToUpdate.getProblemType().getProblemPlace().getPlace();
        ProblemType updatedProblemType = defineProblemType(problemName, problemPlace);
        applicationToUpdate.setProblemType(updatedProblemType);
        applicationDAO.update(applicationToUpdate);
        return new JsonResponse(true);
    }

    public JsonApplicationsList findAllApplications() {
        List<Application> applications = applicationDAO.readAll();
        return applications.isEmpty() ? 
                new JsonApplicationsList(false, 404, "ApplicationsNotFound") : new JsonApplicationsList(true, 200, applications);
    }
}
