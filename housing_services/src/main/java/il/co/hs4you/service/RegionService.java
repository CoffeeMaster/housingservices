package il.co.hs4you.service;

import il.co.hs4you.model.entity.Region;
import il.co.hs4you.persistent.dao.RegionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.0-SNAPSHOT
 * @since 30.09.16.
 */

@Service
public class RegionService {

    @Autowired
    private RegionDAO regionDAO;

    public Region getRegionById(int id){
        return regionDAO.readById(id);
    }

    public List<Region> selectAll(){
        return regionDAO.readAll();
    }

}
