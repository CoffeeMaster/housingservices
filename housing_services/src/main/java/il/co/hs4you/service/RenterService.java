package il.co.hs4you.service;

import il.co.hs4you.model.entity.*;
import il.co.hs4you.persistent.dao.AddressDAO;
import il.co.hs4you.persistent.dao.RegionDAO;
import il.co.hs4you.persistent.dao.RenterDAO;
import il.co.hs4you.persistent.dao.RoleDAO;
import il.co.hs4you.rest.support.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @version 1.0.1-SNAPSHOT
 * @since 10.10.16.
 */

@Service
public class RenterService {

    private static final String EMAIL_PATTERN = "^[^.](\\w|\\.)+@[\\w]+(\\.\\w{2,6}){1,3}";

    @Autowired
    RenterDAO renterDAO;

    @Autowired
    RegionDAO regionDAO;

    @Autowired
    AddressDAO addressDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    PasswordEncoder passwordEncoder;

    public Renter getRenterById(int nationalID){
        return renterDAO.readById(nationalID);
    }

    public boolean register(
            int nationalID,
            @NotNull String name,
            @NotNull String surname,
            @NotNull String password,
            String email,
            String phone
    ){
        if(nationalID == 0)
            return false;
        if(!email.equals("") && !email.matches(EMAIL_PATTERN))
            return false;
        Role userRole = roleDAO.findByName("RENTER");
        if(userRole == null)
            return false;
        // creating Flat stab for not fall on NullPointer
        Flat flatStub = new Flat();
        Building buildingStub = new Building();
        Address addressStub = new Address();
        Region regionStub = new Region();
        addressStub.setRegion(regionStub);
        buildingStub.setAddress(addressStub);
        flatStub.setBuilding(buildingStub);
        
        // creating and filling Renter entity-object
        Renter newRenter = new Renter();
        newRenter.setNationalId(nationalID);
        newRenter.setName(name);
        newRenter.setSurname(surname);
        newRenter.setPassword(passwordEncoder.encode(password));
        newRenter.setEmail(email);
        newRenter.setPhone(phone);
        newRenter.addRole(userRole);
        newRenter.setValidated(false);
        newRenter.setFlat(flatStub);
        renterDAO.create(newRenter);
        // persisting Renter object to DB
        return true;
    }

    public JsonResponse updateRenterProfile(int nationalID, String region, String town, String street, String houseNumber, int flatNumber) {
        if(region.equals("") || town.equals("") || street.equals("") || houseNumber.equals("") || flatNumber == 0)
            return new JsonResponse(false, "updateArgumentsAreEmpty");
        Renter renterToUpdate = renterDAO.readById(nationalID);
        Address foundAddr = addressDAO.findByRestrictions(town, street, houseNumber);
        if(foundAddr!=null){
            for(Flat f : foundAddr.getBuilding().getFlats()){
                if(f.getFlatNumber() == flatNumber) {
                    renterToUpdate.setFlat(f);
                    renterDAO.update(renterToUpdate);
                    return new JsonResponse(true);
                }
            }
            Flat flatToUpdate = renterToUpdate.getFlat();
            flatToUpdate.setFlatNumber(flatNumber);
            flatToUpdate.setBuilding(foundAddr.getBuilding());
            renterDAO.update(renterToUpdate);
            return new JsonResponse(true);
        }
        Region foundReg = regionDAO.findRegionByName(region);
        renterToUpdate.getFlat().setFlatNumber(flatNumber);
        Address addressToUpdate = renterToUpdate.getFlat().getBuilding().getAddress();
        addressToUpdate.setTown(town);
        addressToUpdate.setHouseNumb(houseNumber);
        addressToUpdate.setStreet(street);
        if(foundReg != null)
            addressToUpdate.setRegion(foundReg);
        else
            addressToUpdate.getRegion().setRegionName(region);
        renterDAO.update(renterToUpdate);
        return new JsonResponse(true);
    }

    public JsonResponse changePassword(int nationalID, String oldPassword, String newPassword, String confPasssword){
        Renter renterToUpdate = renterDAO.readById(nationalID);

        String oldPass = renterToUpdate.getPassword();
        oldPassword = passwordEncoder.encode(oldPassword);

        if(!oldPass.equals(oldPassword))
            return new JsonResponse(false, "wrongPassword");
        if(newPassword.equals(confPasssword)){
            renterToUpdate.setPassword(passwordEncoder.encode(newPassword));
            renterDAO.update(renterToUpdate);
            return new JsonResponse(true);
        }
        return new JsonResponse(false, "passwordsMismatch");
    }

    public JsonResponse editPersonalInfo(int currentNationalId, String name, String surname, String phone, String email) {
        Renter renter = renterDAO.readById(currentNationalId);
        if(name != null && !name.equals(""))
            renter.setName(name);
        if(surname != null && !surname.equals(""))
            renter.setSurname(surname);
        if(phone != null && !phone.equals(""))
            renter.setPhone(phone);
        if(email != null && !email.equals(""))
            renter.setEmail(email);
        renterDAO.update(renter);
        return  new JsonResponse(true);
    }

    public JsonResponse updateRenterAddress(int nationalID, String region, String town, String street, String houseNumber, int flatNumber) {
        Renter renterToUpdate = renterDAO.readById(nationalID);

        if(region == null || region.equals(""))
            region = renterToUpdate.getFlat().getBuilding().getAddress().getRegion().getRegionName();
        if(town == null || town.equals(""))
            town = renterToUpdate.getFlat().getBuilding().getAddress().getTown();
        if(street == null || street.equals(""))
            street = renterToUpdate.getFlat().getBuilding().getAddress().getStreet();
        if(houseNumber == null || houseNumber.equals(""))
            houseNumber = renterToUpdate.getFlat().getBuilding().getAddress().getHouseNumb();
        if(flatNumber == 0)
            flatNumber = renterToUpdate.getFlat().getFlatNumber();

        Address foundAddr = addressDAO.findByRestrictions(town, street, houseNumber);
        if(foundAddr!=null){
            for(Flat f : foundAddr.getBuilding().getFlats()){
                if(f.getFlatNumber() == flatNumber) {
                    renterToUpdate.setFlat(f);
                    renterDAO.update(renterToUpdate);
                    return new JsonResponse(true);
                }
            }
            Flat newFlatEntry = new Flat();
            newFlatEntry.setFlatNumber(flatNumber);
            newFlatEntry.setBuilding(foundAddr.getBuilding());
            renterToUpdate.setFlat(newFlatEntry);
            renterDAO.update(renterToUpdate);
            return new JsonResponse(true);
        }

        Region foundRegion = regionDAO.findRegionByName(region);
        Flat newFlatEntry = new Flat();
        Building newBuildingEntry = new Building();
        Address newAddressEntry = new Address();
        newFlatEntry.setFlatNumber(flatNumber);
        newAddressEntry.setTown(town);
        newAddressEntry.setStreet(street);
        newAddressEntry.setHouseNumb(houseNumber);
        if(foundRegion != null)
            newAddressEntry.setRegion(foundRegion);
        else{
            Region newRegionEntry = new Region(region);
            newAddressEntry.setRegion(newRegionEntry);
        }
        newBuildingEntry.setAddress(newAddressEntry);
        newFlatEntry.setBuilding(newBuildingEntry);
        renterToUpdate.setFlat(newFlatEntry);
        renterDAO.update(renterToUpdate);
        return new JsonResponse(true);
    }

    public JsonResponse updateFlatInfo(int nationalId, int rooms, int balconies, double square) {
        Renter renterToUpdate = renterDAO.readById(nationalId);
        Flat flatToUpdate = renterToUpdate.getFlat();
        if(rooms == 0)
            rooms = flatToUpdate.getRooms();
        if(square == 0.d)
            square = flatToUpdate.getSquare();
        flatToUpdate.setRooms(rooms);
        flatToUpdate.setBalconies(balconies);
        flatToUpdate.setSquare(square);
        renterDAO.update(renterToUpdate);
        return new JsonResponse(true);
    }
}
