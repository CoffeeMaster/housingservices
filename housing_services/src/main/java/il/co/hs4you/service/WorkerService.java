package il.co.hs4you.service;

import il.co.hs4you.model.entity.Renter;
import il.co.hs4you.model.entity.Role;
import il.co.hs4you.model.entity.Worker;
import il.co.hs4you.persistent.dao.RenterDAO;
import il.co.hs4you.rest.support.JsonListRespWrapper;
import il.co.hs4you.rest.support.JsonRespWrapper;
import il.co.hs4you.rest.support.JsonResponse;
import org.hibernate.annotations.Fetch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Matvey Mitnitsky
 * @since 11/28/16.
 */

@Service
public class WorkerService {
    
    @Autowired
    RenterDAO renterDAO;
    
    public JsonListRespWrapper<Worker> findAllWorkers() {
        List<Worker> workers = renterDAO.findAllWorkers();
        return workers.isEmpty() ?
                new JsonListRespWrapper<Worker>(false, "UsersWithWorkerRoleNotFound") : new JsonListRespWrapper<Worker>(true, workers);
    }

    public JsonRespWrapper<Worker> findWorkerById(int nationalID) {
            Worker worker =  renterDAO.findWorkerById(nationalID);
            return worker == null ?
                    new JsonRespWrapper<>(false, "WorkerWithSuchIDNotFound") : new JsonRespWrapper<Worker>(true, worker);
    }

    public JsonResponse addWorker(int nationalID) {
        Renter renter = renterDAO.readById(nationalID);
        if(renter == null)
            return new JsonResponse(false, "RenterWithSuchIDNotFound");
        return renterDAO.addWorkerRole(nationalID) ? 
                new JsonResponse(true) : new JsonResponse(false, "failedToUpdateRentersRole");
    }
}
