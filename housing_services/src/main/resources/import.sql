INSERT INTO `hsdb`.`region` (`region_name`) VALUES ('North');
INSERT INTO `hsdb`.`region` (`region_name`) VALUES ('Center');
INSERT INTO `hsdb`.`region` (`region_name`) VALUES ('West');
INSERT INTO `hsdb`.`region` (`region_name`) VALUES ('South');
INSERT INTO `hsdb`.`address` (`town`, `street`, `house_numb`, `region_id`) VALUES ('Petah Tikva', 'Gadera', '10', '2');
INSERT INTO `hsdb`.`address` (`town`, `street`, `house_numb`, `region_id`) VALUES ('Hadera', 'Shakzer', '37', '1');
INSERT INTO `hsdb`.`address` (`town`, `street`, `house_numb`, `region_id`) VALUES ('Ashkelon', 'Eshtol', '15', '4');
INSERT INTO `hsdb`.`building` (`floors`, `found_date`, `last_repair_date`, `address_id`) VALUES ('9', '2001-12-30', '2004-06-15', '1');
INSERT INTO `hsdb`.`building` (`floors`, `found_date`, `address_id`) VALUES ('3', '1999-02-01', '2');
INSERT INTO `hsdb`.`building` (`floors`, `found_date`, `address_id`) VALUES ('6', '1995-11-18', '3');
INSERT INTO `hsdb`.`flat` (`flat_number`, `rooms`, `balconies`, `square`, `building_id`) VALUES ('22', '4', '1', '100.3', '1');
INSERT INTO `hsdb`.`flat` (`flat_number`, `rooms`, `balconies`, `square`, `building_id`) VALUES ('15', '3', '0', '55.5', '2');
INSERT INTO `hsdb`.`flat` (`flat_number`, `rooms`, `balconies`, `square`, `building_id`) VALUES ('10', '4', '1', '115.1', '1');
INSERT INTO `hsdb`.`flat` (`flat_number`, `rooms`, `balconies`, `square`, `building_id`) VALUES ('1', '3', '0', '45.9', '1');
INSERT INTO `hsdb`.`flat` (`flat_number`, `rooms`, `balconies`, `square`, `building_id`) VALUES ('2', '4', '1', '98.8', '1');
INSERT INTO `hsdb`.`flat` (`flat_number`, `rooms`, `balconies`, `square`, `building_id`) VALUES ('10', '2', '1', '45.3', '3');
insert into roles (role_name) values("RENTER");
insert into roles (role_name) VALUES ("ADMIN");
insert into roles (role_name) VALUES ("WORKER");
# password : "john"
INSERT INTO `hsdb`.`renter` (`DTYPE`, `national_id`, `name`, `surname`, `phone`, `email`, `password`, `token`, `flat_id`) VALUES ('Renter', '100000000', 'John', 'Doe', '0577777777', 'john.doe@gmail.com', '$2a$10$NfZrghR.1kkf6Fg.lNR2Z.ElzXhZvwm.GzUJfn8m2KXXlAzrsE5vy', '', '1');
# password : "password"
INSERT INTO `hsdb`.`renter` (`DTYPE`, `national_id`, `name`, `surname`, `phone`, `email`, `password`, `token`, `flat_id`) VALUES ('Renter', '111111111', 'Ploni', 'Almoni', '0596262001', 'matvey.mtn@gmail.com', '$2a$10$/rxGG4VOCYONyhMCikXOhu2pu8CAGPE58qIkUyrNvS.1vBn5BTWuG', '', '1');
# password : "password"
INSERT INTO `hsdb`.`renter` (`DTYPE`, `national_id`, `name`, `surname`, `phone`, `email`, `password`, `token`, `flat_id`) VALUES ('Renter', '222222222', 'Donald', 'Trump', '1001000100', 'tramp@mail.gov.us', '$2a$10$/rxGG4VOCYONyhMCikXOhu2pu8CAGPE58qIkUyrNvS.1vBn5BTWuG', '', '5');
# password : "password"
INSERT INTO `hsdb`.`renter` (`DTYPE`, `national_id`, `name`, `surname`, `phone`, `email`, `password`, `token`, `flat_id`) VALUES ('Renter', '333333333', 'Hilary', 'Clinton', '1110001110', 'hilary@gmail.com', '$2a$10$/rxGG4VOCYONyhMCikXOhu2pu8CAGPE58qIkUyrNvS.1vBn5BTWuG', '', '4');
# password : "admin"
INSERT INTO `hsdb`.`renter` (`DTYPE`, `national_id`, `name`, `surname`, `phone`, `email`, `password`, `token`, `flat_id`) VALUES ('Renter', '1111', 'ADMIN', 'whatever', '', 'admin@hs4you.co.il', '$2a$10$0S.f9qahvNldgYNvl1CLeuGwRy.Vbph3Kb0xWaeA96DbimSEVNmfG', '', '4');
# password : "work"
INSERT INTO `hsdb`.`renter` (`DTYPE`, `national_id`, `name`, `surname`, `phone`, `email`, `password`, `token`, `flat_id`) VALUES ('Worker', '444444444', 'Sergei', 'Nikolayev', '0510100110', 'sergein@hs4u.co.il', '$2a$10$YnI7YRae/xy77r.NlUWSJOK2WzHncOHiYSl6pdhEfT4KnQWy/lMWe', '', '6');
# password : "work"
INSERT INTO `hsdb`.`renter` (`DTYPE`, `national_id`, `name`, `surname`, `phone`, `email`, `password`, `token`, `flat_id`) VALUES ('Worker', '555555555', 'Vasiliy', 'Pop', '0550150115', 'vasilyp@hs4u.co.il', '$2a$10$YnI7YRae/xy77r.NlUWSJOK2WzHncOHiYSl6pdhEfT4KnQWy/lMWe', '', '2');
INSERT INTO renter_roles (renter_national_id, roles_id) VALUES (100000000, 1);
INSERT INTO renter_roles (renter_national_id, roles_id) VALUES (111111111, 1);
INSERT INTO renter_roles (renter_national_id, roles_id) VALUES (222222222, 1);
INSERT INTO renter_roles (renter_national_id, roles_id) VALUES (333333333, 1);
INSERT INTO renter_roles (renter_national_id, roles_id) VALUES (444444444, 1);
INSERT INTO renter_roles (renter_national_id, roles_id) VALUES (444444444, 3);
INSERT INTO renter_roles (renter_national_id, roles_id) VALUES (555555555, 1);
INSERT INTO renter_roles (renter_national_id, roles_id) VALUES (555555555, 3);
INSERT INTO renter_roles (renter_national_id, roles_id) VALUES (1111, 1);
INSERT INTO renter_roles (renter_national_id, roles_id) VALUES (1111, 2);
INSERT INTO `hsdb`.`zip_code` (`zip_code`, `town`) VALUES ('220102', 'Petah Tikva');
INSERT INTO `hsdb`.`zip_code` (`zip_code`, `town`) VALUES ('301330', 'Zichron Yakov');
INSERT INTO `hsdb`.`zip_code` (`zip_code`, `town`) VALUES ('555055', 'Eilat');
INSERT INTO `hsdb`.`zip_code` (`zip_code`, `town`) VALUES ('819813', 'Haifa');
INSERT INTO `hsdb`.`zip_code` (`zip_code`, `town`) VALUES ('775671', 'Jerusalem');
INSERT INTO `hsdb`.`zip_code` (`zip_code`, `town`) VALUES ('111010', 'Tel Aviv');
INSERT INTO `hsdb`.`zip_code` (`zip_code`, `town`) VALUES ('654321', 'Ashkelon');