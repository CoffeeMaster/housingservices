package il.co.hs4you.persistent;

import il.co.hs4you.persistent.config.HibernateConfig;
import il.co.hs4you.persistent.dao.RenterDAO;
import il.co.hs4you.rest.config.MvcConfiguration;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * Created by matvey on 08.10.16.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, MvcConfiguration.class})
public class PersistentConfigTest {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    RenterDAO renterDAO;

    @Test
    public void setSessionFactoryShouldNotBeNull(){
        assertNotNull(sessionFactory);
    }

    @Test
    public void renterDAOShouldNotBeNull(){
        assertNotNull(renterDAO);
    }
 }
